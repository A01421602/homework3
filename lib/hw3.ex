defmodule Hw3 do
  def compute(fname) do
    File.stream!(fname)
    |> CSV.decode(headers: true)
    |> Enum.to_list
    |> Enum.map(fn x -> Map.drop(x, ["Agent Position", "Customer ID", "End Date", "Product", "Resource", "Service Type", "Start Date"]) end)
    |> Enum.group_by(fn x -> x["Case ID"] end)
    |> Map.values()
    |> Enum.map(fn x -> Enum.map(x, fn y -> Map.fetch!(y, "Activity") end) end)
  end
end
